import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'rgba(248,248,248,1)'
  },
  viewActivity: {
      height: 95,
      marginLeft: 5,
      marginRight: 5,
      marginTop: 15,
      backgroundColor: 'white',
      borderRadius: 10
  },
  viewActivity1: {
      flex: 1, 
      flexDirection:'row',
      marginRight: 10,
      alignItems:'center', 
      justifyContent:'center', 
      borderBottomColor: 'rgba(215, 225, 229, 1)', 
      borderBottomWidth: 1,
      height: 50
  },
  viewActivityDetail: {
      marginRight: 1,
      alignItems:'flex-end',
      justifyContent:'center',
      flex:0.65
  },
  viewOragImg: {
      justifyContent:'center',
      alignItems:'center',
      backgroundColor: 'white',
      height: 90
  }
});

export default styles;