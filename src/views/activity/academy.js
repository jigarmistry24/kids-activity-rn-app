import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './styles';
import Activities from '../../components/activities';
import Activity from '../../services/activity-service';

class AcademyScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'اكاديمية الفيصل',
      headerStyle: {
        backgroundColor: 'white',
        shadowColor: 'transparent'
      },
      headerTintColor: 'rgba(247,153,84,1)'
    }
  };

  constructor(props){
    super(props);
    this.state = {
        tab1: {borderColor: 'rgba(247,153,84,1)'},
        tab2: {borderColor: 'white'},
        selectedTab : 'tab1',
        activityData: [],
        academyData : this.props.navigation.state.params.data,
        loading: false
    }
  }

  renderOraganizerInfo() {
      return (
            <ScrollView contentContainerStyle={{marginLeft: 10, marginRight: 10}}>
            <View style={[styles.viewActivity, {height: 125}]}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', fontWeight:'600', marginRight: 10, marginBottom: 10, fontSize: 13}}>
                            المكان
                        </Text>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 10, color:'gray', fontSize: 12}}>
                            {this.state.academyData.city + ", " + this.state.academyData.country}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/location.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', marginBottom: 10, fontSize: 13}}>
                            الهاتف
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.academyData.phone_number}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/phone.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', marginBottom: 10, fontSize: 13}}>
                            الموقع
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.academyData.website}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/glob.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600',  marginBottom: 10, fontSize: 13}}>
                            البريد الالكتروني
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.academyData.email}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/email.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', marginBottom: 10, fontSize: 13}}>
                            تويتر
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10, fontWeight:'600',color:'gray', fontSize: 12}}>
                            {this.state.academyData.twitter}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/twitter.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={[styles.viewActivity, {marginBottom: 20}]}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10, fontWeight:'600', marginBottom: 10, fontSize: 13}}>
                            انستجرام
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.academyData.instagram}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/instagram.png')} />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={[styles.viewActivity, {marginBottom: 20}]}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 15}]}>
                        <Text style={{textAlign:'right', marginRight: 10,fontWeight:'600',  marginBottom: 10, fontSize: 13}}>
                            لينكد ان
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 10, fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.academyData.linkedin}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/linkedin.png')} />
                    </TouchableHighlight>
                </View>
            </View>
          </ScrollView>
      );
  }

  onPressActivities(){
      var self = this;
      const tab1 = {borderColor: 'white'};
      const tab2 = {borderColor: 'rgba(247,153,84,1)'};
      if(this.state.activityData.length > 0) {
          self.setState({tab1, tab2, selectedTab: 'tab2'});
          return;
      }
      self.setState({ loading: true });
      Activity.getAcademyActivities(this.state.academyData._id).then((response)=>{
        self.setState({loading: false});
        if(response.status){
            self.setState({tab1, tab2, selectedTab: 'tab2', activityData: response.data});
        }
      });
  }

  onPressInfo(){
    const tab1 = {borderColor: 'rgba(247,153,84,1)'};
    const tab2 = {borderColor: 'white'};
    this.setState({tab1, tab2, selectedTab: 'tab1'});
  }

  renderTabMenu(){
      return (
        <View style={{flexDirection: 'row', height: 40,
          borderBottomWidth:1, borderBottomColor:'rgba(200,200,200,1)',
          borderTopWidth:1, borderTopColor:'rgba(200,200,200,1)',backgroundColor:'white'}}>
            <View style={{flex:0.15}} />
            <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressActivities()}
                style={{flex:0.35, justifyContent:'center', alignItems:'center'}}>
                <View style={[{alignSelf:'stretch', flex:0.35,justifyContent:'center', alignItems:'center', borderBottomWidth:2}, {borderBottomColor:this.state.tab2.borderColor}]}>
                    <Text style={{fontWeight:'600', fontSize: 15}}>البرامج</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressInfo()}
                style={{flex:0.35, justifyContent:'center', alignItems:'center'}}>
                <View style={[{alignSelf:'stretch', flex:0.35,justifyContent:'center', alignItems:'center', borderBottomWidth:2}, {borderBottomColor:this.state.tab1.borderColor}]}>
                    <Text style={{fontWeight:'600', fontSize: 15}}>معلومات</Text>
                </View>
            </TouchableHighlight>
            <View style={{flex:0.15}}/>
        </View>
      );
  }

  onPressActivity(data) {
    this.props.navigation.navigate('ActivityDetail', {data: data});
  }

  renderTabInfo(){
      if(this.state.selectedTab == 'tab1'){
          return this.renderOraganizerInfo();
      }else{
          return <Activities activityData={this.state.activityData} onPressActivity={(data)=>this.onPressActivity(data)} />;
      }
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner visible={this.state.loading} />
        <View style={styles.viewOragImg}>
            <Image resizeMode={'contain'} style={{width: 60, height: 60}} source={require('../../static/images/oragr.png')} />
        </View>
        {this.renderTabMenu()}
        {this.renderTabInfo()}
      </View>
    );
  }
}

export default AcademyScreen;