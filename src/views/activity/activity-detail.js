import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './styles';
import Activity from '../../services/activity-service';

class ActivityDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'دورة كامبردج',
      headerStyle: {
        backgroundColor: 'white',
        shadowColor: 'transparent'
      },
      headerRight: (
        <View style={{flexDirection: 'row'}}>
            <TouchableHighlight underlayColor='transparent' style={{marginRight:20}} onPress={() => params.onClickFav()}>
                <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/deactive-heart.png')} />
            </TouchableHighlight>
            <TouchableHighlight underlayColor='transparent' style={{marginRight:20}} onPress={() => params.onClickShare()}>
                <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/share.png')} />
            </TouchableHighlight>
        </View>
      ),
      headerTintColor: 'rgba(247,153,84,1)'
    }
  };

  constructor(props){
    super(props);
    this.state = {
        activityData: this.props.navigation.state.params.data,
        loading: false
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onClickShare: this.onClickShare.bind(this),
      onClickFav: this.onClickFav.bind(this)
    });
  }

  onClickFav(){
  }

  onClickShare(){
  }

  onPressOraganizer(){
    var self = this;
    self.setState({ loading: true });
    Activity.getAcademyDetail(this.state.activityData.academy.id).then((response)=>{
      self.setState({loading: false});
      if(response.status){
        self.props.navigation.navigate('Academy', {data: response.data});
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
          <Spinner visible={this.state.loading} />
          <ScrollView contentContainerStyle={{marginLeft: 10, marginRight: 10}}>
              <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 10}]}>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 10, marginBottom: 10, fontSize: 13}}>
                            نبذة عن البرنامج
                        </Text>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 10, color:'gray', fontSize: 12}}>
                          {this.state.activityData.detail}
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/lines.png')} />
                    </TouchableHighlight>
                </View>
              </View>
            <View style={styles.viewActivity1}>
                <View style={{flex:0.10, justifyContent: 'center', alignItems:'center'}}>
                    {/* <IconF name="angle-left" size={24} style={{color: 'rgba(247,153,84,1)'}} /> */}
                </View>
                <Text style={{flex:0.50, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.city + ", " + this.state.activityData.country}
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight: 10, fontSize: 13}}>
                    المكان
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/location.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <TouchableHighlight
                    onPress={()=> this.onPressOraganizer()}
                    underlayColor='transparent'
                    style={{flex:0.10, justifyContent: 'center', alignItems:'center'}}
                >
                    <IconF name="angle-left" size={24} style={{color: 'rgba(247,153,84,1)'}} />
                </TouchableHighlight>
                <Text style={{flex:0.50, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.academy.name}
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600',  marginRight: 10, fontSize: 13}}>
                    الشركة المنظمة
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/computer.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.age}
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600',marginRight: 10, fontSize: 13}}>
                    العمر
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/hash.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.gender}
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight: 10, fontSize: 13}}>
                    النوع
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/profile.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right', fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.activity_date}
                </Text>
                <Text style={{flex:0.30, textAlign:'right', fontWeight:'600', marginRight:10, fontSize: 13}}>
                    التاريخ
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/calendar.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                     {this.state.activityData.activity_time}
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight:10, fontSize: 13}}>
                    الوقت
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/watch.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right', fontWeight:'600', color:'gray', fontSize: 12}}>
                    {this.state.activityData.price} رياليال
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight:10, fontSize: 13}}>
                    الرسوم
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/hand.png')} />
                </TouchableHighlight>
            </View>
          </ScrollView>
      </View>
    );
  }
}

export default ActivityDetail;