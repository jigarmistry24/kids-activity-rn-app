import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
  TextInput,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationActions } from 'react-navigation';

import styles from './styles';
import User from '../../services/user-service';

class RegisterChildScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'هل تود اضافة طفل',
      headerStyle: {
        backgroundColor: 'white',
        shadowColor: 'transparent'
      },
      headerRight: (
        <TouchableHighlight underlayColor='transparent' style={{marginRight:20}} onPress={() => params.onClickSkip()}>
          <Text style={{color: 'rgba(200,200,200,1)'}}>
            تخطي
          </Text>
        </TouchableHighlight>
      ),
      headerTintColor: 'rgba(247,153,84,1)'
    }
  };

  constructor(props){
    super(props);
    this.state = {
      name: '',
      gender: '',
      age: '',
      loading: false,
      registerData: this.props.navigation.state.params.data
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ onClickSkip: this.onClickSkip.bind(this) });
  }

  onClickSkip(){
    this.callRegisterAPI(this.state.registerData);
  }

  onPressAdd(){
    if(this.state.name.length == 0){
      this.showAlert('Please enter the name');
      return;
    }
    if(this.state.gender.length == 0){
      this.showAlert('Please enter the gender');
      return;
    }
    if(this.state.age.length == 0){
      this.showAlert('Please enter the age');
      return;
    }
    let registerData = this.state.registerData;
    registerData['child'] = {
      name: this.state.name,
      gender: this.state.gender,
      age: this.state.age
    }
    this.callRegisterAPI(registerData);
  }

  goToLogin(){
    const navigateAction1 = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Splash'})
      ]
    });
    this.props.navigation.dispatch(navigateAction1);
  }

  callRegisterAPI(data){
    var self = this;
    self.setState({ loading: true });
    User.registerUser(data).then((response)=>{
      self.setState({loading: false});
      if(response.status){
        setTimeout(() => {
          Alert.alert(
            'Kids Activities',
            'Registration Successful, Login to continue',
            [
              {text: 'Ok', onPress: ()=> self.goToLogin()}
            ]
          );
        }, 10);
      }else{
        self.showAlert(response.message);
      }
    });
  }

  showAlert(message){
    setTimeout(() => {
      Alert.alert(
        'Kids Activities',
        message,
        [
          {text: 'Ok'}
        ]
      );
    }, 10);
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner visible={this.state.loading} />
        <View style={[styles.inputContainer, {marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="العمر"
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.name}
            onChangeText={(text) => this.setState({ name: text })}
            editable
          />
        </View>
        <View style={[styles.inputContainer, {marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="النوع "
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.gender}
            onChangeText={(text) => this.setState({ gender: text })}
            editable
          />
        </View>
        <View style={[styles.inputContainer, {marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="الاهتمامات "
            keyboardType='numeric'
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.age}
            onChangeText={(text) => this.setState({ age: text })}
            editable
          />
        </View>
        <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressAdd()} style={styles.btnSignUp}>
          <Text style={{color:'white', fontWeight:'600', fontSize: 15}}>اضف</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default RegisterChildScreen;