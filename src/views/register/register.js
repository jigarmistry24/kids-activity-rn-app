import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
  TextInput,
  ScrollView,
  TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';

import styles from './styles';

class RegisterScreen extends Component {
  static navigationOptions = {
    title: 'تسجيل عضو جديد',
    headerStyle: {
      backgroundColor: 'white',
      shadowColor: 'transparent'
    },
    headerTintColor: 'rgba(247,153,84,1)'
  };

  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      mobilePrefix: '',
      mobileNumber: '',
      password: '',
      country: '',
      city: ''
    }
  }

  onPressSignUp(){
    if(this.state.name.length == 0){
      this.showAlert('Please enter the name');
      return;
    }
    if(this.state.email.length == 0){
      this.showAlert('Please enter the email');
      return;
    }
    if(this.state.mobileNumber.length == 0){
      this.showAlert('Please enter the mobile number');
      return;
    }
    if(this.state.city.length == 0) {
      this.showAlert('Please enter the city');
      return;
    }
    if(this.state.country.length == 0){
      this.showAlert('Please enter the country');
      return;
    }
    if(this.state.password.length == 0){
      this.showAlert('Please enter the password');
      return;
    }
    if(this.state.mobilePrefix == 0) {
      this.showAlert('Please enter the mobile prefix');
      return;
    }
    let registerData = {
      name: this.state.name,
      email: this.state.email,
      mobile_number: this.state.mobilePrefix + this.state.mobileNumber,
      city: this.state.city,
      country: this.state.country,
      password: this.state.password
    }
    this.props.navigation.navigate('RegisterChild', {data: registerData});
  }

  showAlert(message){
    Alert.alert(
      'Kids Activities',
      message,
      [
        {text: 'Ok'}
      ]
    )
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View style={[styles.inputContainer, {marginBottom: 25, marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="العمر"
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.name}
            onChangeText={(text) => this.setState({ name: text })}
            editable
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="البريد الالكتروني "
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.email}
            onChangeText={(text) => this.setState({ email: text })}
            editable
          />
        </View>
        <View style={{height: 45, flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:25}}>
            <View style={[styles.inputContainer, {flex:0.20, marginLeft:40, marginRight:0}]}>
                <TextInput
                    style={styles.input}
                    placeholder="91+"
                    returnKeyType={"next"}
                    autoCapitalize="none"
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="rgba(200,200,200,1)"
                    value={this.state.mobilePrefix}
                    onChangeText={(text) => this.setState({ mobilePrefix: text })}
                    editable
                />
            </View>
            <View style={[styles.inputContainer, {flex:0.80, marginLeft:10, marginRight:40}]}>
                <TextInput
                    style={styles.input}
                    placeholder="رقم الجوال "
                    returnKeyType={"next"}
                    autoCapitalize="none"
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="rgba(200,200,200,1)"
                    value={this.state.mobileNumber}
                    onChangeText={(text) => this.setState({ mobileNumber: text })}
                    editable
                />
            </View>
        </View>
        <TouchableHighlight style={[styles.inputContainer, {marginTop: 25, borderWidth:0}]}>
            <View style={[styles.inputContainer, {marginLeft:0, marginRight:0}]}>
                {/* <IconF name="caret-down" size={22} style={styles.downArrow} /> */}
                <TextInput
                    style={styles.input}
                    placeholder="الدولة "
                    returnKeyType={"next"}
                    autoCapitalize="none"
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="rgba(200,200,200,1)"
                    value={this.state.country}
                    onChangeText={(text) => this.setState({ country: text })}
                    editable={true}
                />
            </View>
        </TouchableHighlight>
        <TouchableHighlight style={[styles.inputContainer, {marginTop: 25, borderWidth:0}]}>
            <View style={[styles.inputContainer, {marginLeft:0, marginRight:0}]}>
                {/* <IconF name="caret-down" size={22} style={styles.downArrow} /> */}
                <TextInput
                    style={styles.input}
                    placeholder="المدينة "
                    returnKeyType={"next"}
                    autoCapitalize="none"
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="rgba(200,200,200,1)"
                    value={this.state.city}
                    onChangeText={(text) => this.setState({ city: text })}
                    editable={true}
                />
            </View>
        </TouchableHighlight>
        <View style={[styles.inputContainer, {marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="مشاركة الموقع "
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.password}
            onChangeText={(text) => this.setState({ password: text })}
            editable
            secureTextEntry
          />
        </View>
        <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressSignUp()} style={styles.btnSignUp}>
          <Text style={{color:'white', fontWeight:'600', fontSize: 15}}>تسجيل عضوية</Text>
        </TouchableHighlight>
      </ScrollView>
    );
  }
}

export default RegisterScreen;