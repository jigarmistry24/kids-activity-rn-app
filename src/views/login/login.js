import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  Alert,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationActions } from 'react-navigation';

import styles from './styles';
import User from '../../services/user-service';

class LoginScreen extends Component {
  static navigationOptions = {
    title: 'الدخول',
    headerStyle: {
      backgroundColor: 'white',
      shadowColor: 'transparent'
    },
    headerTintColor: 'rgba(247,153,84,1)'
  };

  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false
    }
  }

  goToHome(data){
    AsyncStorage.setItem('userData', JSON.stringify(data));
    this.props.navigation.navigate('Home');
  }

  onPressLogin(){
    if(this.state.email.length == 0){
      this.showAlert('Please enter the email');
      return;
    }
    if(this.state.password.length == 0){
      this.showAlert('Please enter the password');
      return;
    }
    let data = {
      email: this.state.email,
      password: this.state.password
    }

    var self = this;
    self.setState({ loading: true });
    User.loginUser(data).then((response)=>{
      self.setState({loading: false});
      if(response.status){
        setTimeout(() => {
          Alert.alert(
            'Kids Activities',
            response.message,
            [
              {text: 'Ok', onPress: ()=> self.goToHome(response.data)}
            ]
          );
        }, 10);
      }else{
        self.setState({password: ''});
        self.showAlert(response.message);
      }
    });
  }

  onPressSignUp(){
    this.props.navigation.navigate('Register');
  }

  showAlert(message){
    setTimeout(() => {
      Alert.alert(
        'Kids Activities',
        message,
        [
          {text: 'Ok'}
        ]
      );
    }, 10);
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner visible={this.state.loading} />
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="بريد الكتروني "
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.email}
            onChangeText={(text) => this.setState({ email: text })}
            editable
          />
        </View>
        <View style={[styles.inputContainer, {marginTop: 25}]}>
          <TextInput
            style={styles.input}
            placeholder="كلمة مرور "
            returnKeyType={"next"}
            autoCapitalize="none"
            underlineColorAndroid={'transparent'}
            placeholderTextColor="rgba(200,200,200,1)"
            value={this.state.password}
            onChangeText={(text) => this.setState({ password: text })}
            editable
            secureTextEntry
          />
        </View>
        <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressLogin()} style={styles.btnLogin}>
          <Text style={{color:'white', fontWeight:'600', fontSize: 15}}>تسجيل</Text>
        </TouchableHighlight>
        {/* <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressLogin()} style={{marginTop:25}}>
          <Text style={{color:'rgba(200,200,200,1)',fontWeight:'600', fontSize: 15}}>نسيت كلمة المرور</Text>
        </TouchableHighlight> */}
        <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressSignUp()} style={{marginTop:45}}>
          <Text style={{color:'rgba(200,200,200,1)', fontWeight:'600', fontSize: 15}}>ليس لديك حساب سجل الان</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default LoginScreen;