import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputContainer: {
    marginLeft: 40,
    marginRight: 40,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#CCC',
    borderRadius: 10
  },
  input: {
    marginRight: 10,
    left: 0,
    top: 0,
    right: 0,
    height: 45,
    fontSize: 14,
    paddingBottom: 0,
    paddingTop: 0,
    textAlign: 'right',
    fontWeight:'600'
  },
  btnLogin: {
    marginTop: 25,
    marginLeft: 40,
    marginRight: 40,
    height: 45,
    backgroundColor:'rgba(247,153,84,1)',
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 8,
    alignSelf: 'stretch'
  }
});

export default styles;