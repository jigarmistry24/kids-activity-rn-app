import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './styles';

import Activities from '../../components/activities';
import BottomMenu from '../../components/bottomMenu';
import Activity from '../../services/activity-service';

class FavouriteScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'تفضيلاتي',
      headerStyle: {
        backgroundColor: 'white',
        shadowColor: 'transparent'
      },
      headerTintColor: 'rgba(247,153,84,1)'
    }
  };

  constructor(props){
    super(props);
    const activityData = [];
    this.state = {
      showData: false,
      loading: false,
      activityData: activityData,
      favIds: []
    };
  }

  componentDidMount(){
    AsyncStorage.getItem('favIds').then((data) => {
        if(data){
            let favIds = JSON.parse(data);
            this.setState({favIds: favIds});
        }
        this.callActivityAPI();
    });
  }

  callActivityAPI(){
    var self = this;
    self.setState({ loading: true });
    let data = {"activity_ids": this.state.favIds};
    Activity.getBlukActivites(data).then((response)=>{
      self.setState({loading: false});
      if(response.status){
        self.setState({activityData: response.data, showData: true});
      }
    });
  }

  onPressTab(tab){
    switch(tab){
      case 1:
        const navigateAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Home'})
          ]
        });
        this.props.navigation.dispatch(navigateAction);
        break;
      case 2:
        const navigateAction2 = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Blog'})
          ]
        });
        this.props.navigation.dispatch(navigateAction2);
        break;
    }
  }

  onPressActivity(data) {
    this.props.navigation.navigate('ActivityDetail', {data: data});
  }

  render() {
    return (
      <View style={styles.container}>
          <Spinner visible={this.state.loading} />
          {this.state.showData &&
            <Activities activityData={this.state.activityData} onPressActivity={(data)=>this.onPressActivity(data)} fromFav={true} />
          }
          <BottomMenu selectedTab={3} onPressTab={(tab)=>this.onPressTab(tab)} />
      </View>
    );
  }
}

export default FavouriteScreen;