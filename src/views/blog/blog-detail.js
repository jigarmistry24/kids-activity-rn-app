import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight
} from 'react-native';

var windowSize = Dimensions.get('window');

import styles from './styles';

import { NavigationActions } from 'react-navigation';

class BlogDetailScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: navigation.state.params.data.title,
            headerStyle: {
            backgroundColor: 'white',
                shadowColor: 'transparent'
            },
            headerRight: (
                <TouchableHighlight underlayColor='transparent' style={{marginRight:20}} onPress={() => params.onClickShare()}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/share-blog.png')} />
                </TouchableHighlight>
            ),
            headerTintColor: 'rgba(247,153,84,1)'
        }
    };

    constructor(props){
        super(props);
        this.state = {
            blogData : this.props.navigation.state.params.data
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onClickShare: this.onClickShare.bind(this),
        });
    }

    onClickShare(){
    }

    render() {
        return (
            <View style={styles.viewBlog}>
                <View style={{flex:1, flexDirection:'column',  margin: 10}}>
                    <TouchableHighlight style={{flex:0.80, borderRadius: 10}}>
                        <Image
                            resizeMode={'cover'}
                            style={{flex:1, width: null, height: null}}
                            source={require('../../static/images/blog1.png')} />
                    </TouchableHighlight>
                    <View style={{flex:0.20}}>
                        <Text style={{textAlign:'right', marginRight: 20, fontWeight:'600', marginBottom: 10, fontSize: 15, marginTop: 10}}>
                            {this.state.blogData.title}
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 20, fontWeight:'600', marginBottom: 10, fontSize: 13, marginTop: 10}}>
                           {new Date(this.state.blogData.created_date).toDateString()}
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 20,fontWeight:'600', color:'gray', fontSize: 12}}>
                            {this.state.blogData.detail}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

export default BlogDetailScreen;