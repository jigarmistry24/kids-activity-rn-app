import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  ListView,
  StyleSheet,
  Dimensions,
  TouchableHighlight
} from 'react-native';

var windowSize = Dimensions.get('window');
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './styles';
import Blog from '../../services/blog-service';

import BottomMenu from '../../components/bottomMenu';
import { NavigationActions } from 'react-navigation';

class BlogScreen extends Component {

    static navigationOptions = {
        title: 'مدونة',
        headerStyle: {
        backgroundColor: 'white',
            shadowColor: 'transparent'
        },
        headerTintColor: 'rgba(247,153,84,1)'
    };

    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        const blogData = [];
        this.state = {
            listViewKey : Math.random(),
            ds: ds,
            loading: false,
            showData: false,
            blogDataSource: ds.cloneWithRows(blogData),
        }
    }

    componentDidMount(){
        this.callBlogAPI();
    }

    callBlogAPI(){
        var self = this;
        self.setState({ loading: true });
        Blog.getBlogs().then((response)=>{
            self.setState({loading: false});
            if(response.status){
                self.setState({
                    blogDataSource: self.state.ds.cloneWithRows(response.data),
                    showData: true
                });
            }
        });
    }

    onPressBlog(data){
        this.props.navigation.navigate('BlogDetail', {data: data});
    }

    renderBlogs(data, rowId){
        return (
            <View style={styles.viewBlog}>
                <Spinner visible={this.state.loading} />
                <View style={{flex:1, flexDirection:'column',  margin: 10}}>
                    <TouchableHighlight style={{flex:0.80, borderRadius: 10}} onPress={()=>this.onPressBlog(data)}>
                        <Image
                            resizeMode={'cover'}
                            style={{flex:1, width: null, height: null}}
                            source={require('../../static/images/blog1.png')} />
                    </TouchableHighlight>
                    <View style={{flex:0.20}}>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 20, fontSize: 13, marginTop: 10}}>
                            {data.title}
                        </Text>
                        <Text style={{textAlign:'right', marginRight: 20, fontWeight:'600', marginBottom: 10, fontSize: 13, marginTop: 10}}>
                            {new Date(data.created_date).toDateString()}
                        </Text>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 20, color:'gray', fontSize: 12}}>
                            {data.description}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    onPressTab(tab){
      switch(tab){
        case 3:
            const navigateAction1 = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Favourite'})
                ]
            });
            this.props.navigation.dispatch(navigateAction1);
            break;
        case 1:
            const navigateAction2 = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home'})
                ]
            });
            this.props.navigation.dispatch(navigateAction2);
            break;
        }
    }

    render() {
        return (
            <View style={{flex:1}}>
                {this.state.showData &&
                    <ListView
                        key={this.state.listViewKey}
                        dataSource={this.state.blogDataSource}
                        removeClippedSubviews={false}
                        enableEmptySections
                        renderRow={(data, sectionId, rowId) => this.renderBlogs(data, rowId)}
                    />
                }
                <View style={{height: 60}} />
                <BottomMenu selectedTab={2} onPressTab={(tab)=>this.onPressTab(tab)}/>
            </View>
        );
    }
}

export default BlogScreen;