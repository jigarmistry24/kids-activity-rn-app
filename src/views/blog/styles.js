import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';

var windowSize = Dimensions.get('window');

const styles = StyleSheet.create({
    viewBlog: {
        height: 180,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 50,
    }
});

export default styles;