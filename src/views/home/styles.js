import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';

var windowSize = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(248,248,248,1)'
  },
  searchContainer: {
    backgroundColor: 'rgba(248,248,248,1)'
  },
  sortModal: {
    alignItems: 'center',
    width: windowSize.width,
    height: 200
  },
  categoryModal: {
    width: windowSize.width - 20,
    height: windowSize.height - 120
  },
  inputContainer: {
    marginLeft: 20,
    marginRight: 20,
    alignSelf: 'stretch',
    borderRadius: 10,
    backgroundColor: 'white'
  },
  input: {
    marginRight: 10,
    left: 0,
    top: 0,
    right: 0,
    height: 45,
    fontSize: 14,
    paddingBottom: 0,
    paddingTop: 0,
    textAlign: 'right',
    fontWeight:'600'
  },
  btnResults: {
    marginTop: 25,
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 30,
    height: 45,
    backgroundColor:'rgba(247,153,84,1)',
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 8,
    alignSelf: 'stretch'
  },
  viewActivity: {
      height: 85,
      marginLeft: 5,
      marginRight: 5,
      marginTop: 15,
      backgroundColor: 'white',
      borderRadius: 10
  },
  viewActivity1: {
      flex: 1, 
      flexDirection:'row',
      marginRight: 10,
      alignItems:'center', 
      justifyContent:'center', 
      borderBottomColor: 'rgba(215, 225, 229, 1)', 
      borderBottomWidth: 1,
      height: 50
  },
  viewActivityDetail: {
      marginRight: 1,
      alignItems:'flex-end',
      justifyContent:'center',
      flex:0.65
  },
  viewOragImg: {
      justifyContent:'center',
      alignItems:'center',
      backgroundColor: 'white',
      height: 90
  }
});

export default styles;