import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ListView,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import Modal from 'react-native-modalbox';
import IconF from 'react-native-vector-icons/FontAwesome';
import Checkbox from 'react-native-custom-checkbox';
import MultiSlider from '@ptomasroos/react-native-multi-slider';


import { NavigationActions } from 'react-navigation';
var windowSize = Dimensions.get('window');

import styles from './styles';

class SearchScreen extends Component {
  static navigationOptions = {
    title: 'البرامج',
    headerStyle: {
      backgroundColor: 'white',
      shadowColor: 'transparent'
    },
    headerTintColor: 'rgba(247,153,84,1)'
  };

  constructor(props){
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    const categoryData = [{id:1, name: 'البرمجة'},{id:2, name:'الجرافيك'},{id:3, name:'المحاسبة'},{id:4, name:'مجال ما'}];

    this.state = {
        keyword: '',
        isCategoryModalOpen: false,
        listViewKey : Math.random(),
        ds:ds,
        categoryDataSource: ds.cloneWithRows(categoryData),
    }
  }

  onPressResults(){

  }

  onPressCategory(){
    this.setState({isCategoryModalOpen: true});
  }

  onPressCloseFromCategoryModal(){
    this.setState({isCategoryModalOpen: false});
  }

  renderCategories(data, rowId){
    return (
        <View style={{flexDirection:'row', height: 40, borderBottomColor:'rgba(200,200,200,1)', 
        borderBottomWidth:1, marginLeft: 20, marginRight:20}}>
            <View style={{flex:0.75, justifyContent: 'center', alignItems:'flex-end'}}>
                <Text style={{textAlign:'right', marginRight: 15, fontSize: 13, fontWeight:'600'}}>
                    {data.name}
                </Text>
            </View>
            <View style={{flex:0.25, justifyContent: 'center', alignItems:'center'}}>
                <Checkbox
                    checked={true}
                    style={{backgroundColor: 'white', color:'rgba(247,153,84,1)', borderRadius: 5,
                            borderWidth: 1, borderColor:'rgba(200,200,200,1)',  margin: 5}}
                    onChange={(name, checked) => alert(data.name)}/>
            </View>
        </View>
    );
  }

  renderCategoryModal(){
      return (
          <Modal
            style={styles.categoryModal}
            swipeToClose={false}
            onClosed={()=>this.setState({isCategoryModalOpen: false})}
            isOpen={this.state.isCategoryModalOpen}
            position={"center"}
          >
            <View style={{flexDirection:'row', height: 40}}>
                <TouchableHighlight 
                        onPress={()=> this.onPressCloseFromCategoryModal()}
                        underlayColor="transparent" 
                        style={{flex:0.20, justifyContent: 'center', alignItems:'center'}}
                    >
                        <IconF name="close" size={24} style={{color: 'black'}} />
                </TouchableHighlight>
                <View style={{flex:0.80, justifyContent: 'center', alignItems:'flex-start'}}>
                    <Text style={{color:'rgba(247,153,84,1)', fontWeight:'600', fontSize: 15}}>اختر المجال</Text>
                </View>        
            </View>
            <View style={{height: 15}} />
            <ListView
                key={this.state.listViewKey}
                dataSource={this.state.categoryDataSource}
                removeClippedSubviews={false}
                renderRow={(data, sectionId, rowId) => this.renderCategories(data, rowId)}
            />
          </Modal>
      );
  }

  render() {
    return (
      <View>  
        <ScrollView contentContainerStyle={styles.searchContainer}>
            <View style={[styles.inputContainer, {marginTop: 20}]}>
            <TextInput
                style={styles.input}
                placeholder="ادخل كلمات البحث"
                returnKeyType={"next"}
                autoCapitalize="none"
                underlineColorAndroid={'transparent'}
                placeholderTextColor="rgba(200,200,200,1)"
                value={this.state.keyword}
                onChangeText={(text) => this.setState({ keyword: text })}
                editable
            />
            </View>
            <View style={[styles.inputContainer, {marginTop: 20, height: 50, margin: 10}]}>
                <View style={{flexDirection:'row', justifyContent: 'center', alignItems:'center', height: 50}}>
                    <View style={{flex:0.50, flexDirection:'row', justifyContent: 'center', alignItems:'center', marginLeft:10}}>
                    <TouchableHighlight 
                            onPress={()=> this.onPressCategory()}
                            underlayColor="transparent" 
                            style={{flex:0.15, justifyContent: 'center', alignItems:'center'}}
                        >
                            <IconF name="angle-down" size={24} style={{color: 'rgba(247,153,84,1)'}} />
                    </TouchableHighlight> 
                    <TouchableHighlight 
                            onPress={()=> this.onPressCategory()}
                            underlayColor="transparent" 
                            style={{flex:0.65, justifyContent: 'center', alignItems:'flex-end'}}
                        >
                            <Text style={{textAlign:'right', fontWeight:'600', alignSelf:'flex-end', fontSize:13, color:'rgba(200,200,200,1)'}}>المجال</Text>
                    </TouchableHighlight>
                    <TouchableHighlight 
                            onPress={()=> this.onPressCategory()}
                            underlayColor="transparent"     
                            style={{flex:0.20, justifyContent: 'center', alignItems:'center'}}
                        >
                            <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/personwb.png')} />
                    </TouchableHighlight>   
                    </View>
                    <View style={{flex:0.05}} />        
                    <View style={{flex:0.50, flexDirection:'row', justifyContent: 'center', alignItems:'center', marginRight:10}}>
                    <View style={{flex:0.15, justifyContent: 'center', alignItems:'center'}}>
                            <IconF name="angle-down" size={24} style={{color: 'rgba(247,153,84,1)'}} />
                    </View> 
                    <View style={{flex:0.65, justifyContent: 'center', alignItems:'flex-end'}}>
                            <Text style={{textAlign:'right',fontWeight:'600', alignSelf:'flex-end', fontSize:13, color:'rgba(200,200,200,1)'}}>اختر المدينة</Text>
                    </View>
                    <View style={{flex:0.20, justifyContent: 'center', alignItems:'center'}}>
                            <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/school.png')} />
                    </View>
                    </View>
                </View>    
            </View>
            <View style={[styles.inputContainer, {marginTop: 20, height: 50}]}>
                <View style={{flexDirection:'row', justifyContent: 'center', alignItems:'center', height: 50}}>
                    <View style={{flex: 0.30}}/>
                    <View style={{flex: 0.25, justifyContent: 'center', alignItems:'center', flexDirection:'row'}}>
                        <Text style={{textAlign:'right',fontWeight:'600', fontSize:13, color:'rgba(200,200,200,1)'}}>انثي</Text>
                        <Checkbox
                            checked={true}
                            style={{backgroundColor: 'white', color:'rgba(247,153,84,1)', borderRadius: 5,
                                    borderWidth: 1, borderColor:'rgba(200,200,200,1)',  margin: 5}}
                            onChange={(name, checked) => alert('female')}/>
                    </View>
                    <View style={{flex: 0.25, justifyContent: 'center', alignItems:'center', flexDirection:'row'}}>
                        <Text style={{textAlign:'right', fontSize:13, color:'rgba(200,200,200,1)'}}>ذكر</Text>
                        <Checkbox
                            checked={true}
                            style={{backgroundColor: 'white', color:'rgba(247,153,84,1)', borderRadius: 5,
                                    borderWidth: 1, borderColor:'rgba(200,200,200,1)',  margin: 5}}
                            onChange={(name, checked) => alert('male')}/>
                    </View>
                    <View style={{flex: 0.20, justifyContent: 'center', alignItems:'center'}}>
                        <Text style={{textAlign:'right', fontWeight:'600', fontSize:13, color:'black'}}>النوع</Text>
                    </View>    
                </View>    
            </View>    
            <View style={[styles.inputContainer, {marginTop: 20, height: 90}]}>
                <View style={{flexDirection:'column', height: 90}}>
                    <View style={{alignItems:'flex-end', flex:0.50}}>
                        <Text style={{alignSelf:'flex-end', marginRight: 20,fontWeight:'600'}}>السعر</Text>
                    </View>    
                    <View style={{flex:0.50}}>
                        <MultiSlider 
                            values={[4,10]} 
                            sliderLength={windowSize.width - 80} 
                            selectedStyle={{backgroundColor: 'rgba(247,153,84,1)' }}
                            markerStyle = {{backgroundColor:'white', borderColor:'rgba(247,153,84,1)', borderWidth:1}}
                            containerStyle={{height: 30, marginTop: 20, marginLeft:20, marginRight:20}}/>
                    </View>    
                </View>    
            </View>
            <View style={[styles.inputContainer, {marginTop: 20, height: 90}]}>
                <View style={{flexDirection:'column', height: 90}}>
                    <View style={{alignItems:'flex-end', flex:0.50}}>
                        <Text style={{alignSelf:'flex-end', marginRight: 20,fontWeight:'600'}}>العمر</Text>
                    </View>    
                    <View style={{flex:0.50}}>
                        <MultiSlider 
                            values={[2,10]} 
                            sliderLength={windowSize.width - 80} 
                            selectedStyle={{backgroundColor: 'rgba(247,153,84,1)' }}
                            markerStyle = {{backgroundColor:'white', borderColor:'rgba(247,153,84,1)', borderWidth:1}}
                            containerStyle={{height: 30, marginTop: 20, marginLeft:20, marginRight:20}}/>
                    </View>    
                </View> 
            </View>    
            <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressResults()} style={styles.btnResults}>
                <Text style={{color:'white',fontWeight:'600', fontSize: 15}}>إظهر البحث</Text>
            </TouchableHighlight>
        </ScrollView>
        {this.renderCategoryModal()}
      </View>
    );
  }
}

export default SearchScreen;