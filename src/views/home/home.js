import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableHighlight,
  ScrollView
} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Modal from 'react-native-modalbox';
import Icon from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './styles';
import Activities from '../../components/activities';
import BottomMenu from '../../components/bottomMenu';
import Activity from '../../services/activity-service';

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'دليل خدمات',
      headerStyle: {
        backgroundColor: 'white',
        shadowColor: 'transparent'
      },
      headerRight: (
        <TouchableHighlight underlayColor='transparent' style={{marginRight:20}} onPress={() => params.onClickFilter()}>
          <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/filter.png')} />
        </TouchableHighlight>
      ),
      headerLeft: (
        <TouchableHighlight underlayColor='transparent' style={{marginLeft:20}} onPress={() => params.onClickSort()}>
          <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/sort.png')} />
        </TouchableHighlight>
      ),
      headerTintColor: 'rgba(247,153,84,1)'
    }
  };

  constructor(props){
    super(props);
    this.state = {
      tab1: {borderColor: 'rgba(247,153,84,1)'},
      tab2: {borderColor: 'white'},
      selectedTab : 'tab1',
      activityData: [],
      isSortModalOpen: false,
      loading: false,
      showData: false
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onClickFilter: this.onClickFilter.bind(this),
      onClickSort: this.onClickSort.bind(this)
    });
    this.callActivityAPI();
  }

  callActivityAPI(){
    var self = this;
    self.setState({ loading: true });
    Activity.getActivities().then((response)=>{
      self.setState({loading: false});
      if(response.status){
        self.setState({activityData: response.data, showData: true});
      }
    });
  }

  onClickFilter(){
    this.props.navigation.navigate('Search');
  }

  onClickSort(){
    this.setState({isSortModalOpen: true});
  }

  onPressOraganizer(){
    this.props.navigation.navigate('Academy');
  }

  onPressTab(tab){
    switch(tab){
      case 3:
        const navigateAction1 = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Favourite'})
          ]
        });
        this.props.navigation.dispatch(navigateAction1);
        break;
      case 2:
        const navigateAction2 = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Blog'})
          ]
        });
        this.props.navigation.dispatch(navigateAction2);
        break;
    }
  }

  onPressSelected(){
      const tab1 = {borderColor: 'white'};
      const tab2 = {borderColor: 'rgba(247,153,84,1)'};
      this.setState({tab1, tab2, selectedTab: 'tab2'});
  }

  onPressNew(){
    const tab1 = {borderColor: 'rgba(247,153,84,1)'};
    const tab2 = {borderColor: 'white'};
    this.setState({tab1, tab2, selectedTab: 'tab1'});
  }

  renderTabMenu(){
      return (
        <View style={{flexDirection: 'row', height: 40,
          borderBottomWidth:1, borderBottomColor:'rgba(200,200,200,1)',
          borderTopWidth:1, borderTopColor:'rgba(200,200,200,1)',backgroundColor:'white'}}>
            <View style={{flex:0.15}} />
            <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressSelected()}
                style={{flex:0.35, justifyContent:'center', alignItems:'center'}}>
                <View style={[{alignSelf:'stretch', flex:0.35,justifyContent:'center', alignItems:'center', borderBottomWidth:2}, {borderBottomColor:this.state.tab2.borderColor}]}>
                    <Text style={{fontWeight:'600', fontSize: 15}}>اخترنا لك</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressNew()}
                style={{flex:0.35, justifyContent:'center', alignItems:'center'}}>
                <View style={[{alignSelf:'stretch', flex:0.35,justifyContent:'center', alignItems:'center', borderBottomWidth:2}, {borderBottomColor:this.state.tab1.borderColor}]}>
                    <Text style={{fontWeight:'600', fontSize: 15}}>الجديد</Text>
                </View>
            </TouchableHighlight>
            <View style={{flex:0.15}}/>
        </View>
      );
  }

  onPressActivity(data) {
    this.props.navigation.navigate('ActivityDetail', {data: data});
  }

  renderTabInfo(){
      if(this.state.selectedTab == 'tab2'){
          return (
            <ScrollView contentContainerStyle={{marginLeft: 10, marginRight: 10}}>
              <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <View style={[styles.viewActivityDetail, {flex:0.90, justifyContent: 'flex-start', marginTop: 10}]}>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 10, marginBottom: 10, fontSize: 13}}>
                            نبذة عن البرنامج
                        </Text>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 10, color:'gray', fontSize: 12}}>
                           اكاديمية فيصل #1
                        </Text>
                    </View>
                    <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', marginTop:10, alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/lines.png')} />
                    </TouchableHighlight>
                </View>
              </View>
            <View style={styles.viewActivity1}>
                <View style={{flex:0.10, justifyContent: 'center', alignItems:'center'}}>
                    <IconF name="angle-left" size={24} style={{color: 'rgba(247,153,84,1)'}} />
                </View>
                <Text style={{flex:0.50, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    دولة السعودية مدينة الرياض
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight: 10, fontSize: 13}}>
                    المكان
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/location.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <TouchableHighlight
                    onPress={()=> this.onPressOraganizer()}
                    underlayColor='transparent'
                    style={{flex:0.10, justifyContent: 'center', alignItems:'center'}}
                >
                    <IconF name="angle-left" size={24} style={{color: 'rgba(247,153,84,1)'}} />
                </TouchableHighlight>
                <Text style={{flex:0.50, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    اكاديمية فيصل
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600',  marginRight: 10, fontSize: 13}}>
                    الشركة المنظمة
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/computer.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    من 15 عام الي 35
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600',marginRight: 10, fontSize: 13}}>
                    العمر
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/hash.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    ذكر و انثي
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight: 10, fontSize: 13}}>
                    النوع
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/profile.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right', fontWeight:'600', color:'gray', fontSize: 12}}>
                    ذكر و انثي #1
                </Text>
                <Text style={{flex:0.30, textAlign:'right', fontWeight:'600', marginRight:10, fontSize: 13}}>
                    التاريخ
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/calendar.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right',fontWeight:'600', color:'gray', fontSize: 12}}>
                    ذكر و انثي #2
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight:10, fontSize: 13}}>
                    الوقت
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/watch.png')} />
                </TouchableHighlight>
            </View>
            <View style={styles.viewActivity1}>
                <Text style={{flex:0.60, textAlign:'right', fontWeight:'600', color:'gray', fontSize: 12}}>
                    120 رياليال
                </Text>
                <Text style={{flex:0.30, textAlign:'right',fontWeight:'600', marginRight:10, fontSize: 13}}>
                    الرسوم
                </Text>
                <TouchableHighlight style={{flex:0.10, justifyContent: 'flex-start', alignItems:'center'}}>
                    <Image resizeMode={'contain'} style={{width: 20, height: 20}} source={require('../../static/images/hand.png')} />
                </TouchableHighlight>
            </View>
          </ScrollView>
          );
      }else{
          return <Activities activityData={this.state.activityData} onPressActivity={(data)=>this.onPressActivity(data)} />;;
      }
  }

  onPressSortOption(){
    this.setState({isSortModalOpen: false});
  }

  renderSortModal(){
      return (
          <Modal
            style={styles.sortModal}
            swipeToClose={false}
            onClosed={()=>this.setState({isSortModalOpen: false})}
            isOpen={this.state.isSortModalOpen}
            position={"bottom"}
          >
            <View style={{margin: 10, flexDirection:'column'}}>
              <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressSortOption()}
                style={{flex:0.30, justifyContent:'center', alignItems:'center', borderBottomColor:'rgba(200,200,200,1)', borderBottomWidth: 1}}>
                <Text style={{color: 'gray', fontWeight:'600'}}>الأقرب مكانا</Text>
              </TouchableHighlight>
              <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressSortOption()}
                style={{flex:0.30,justifyContent:'center', alignItems:'center', borderBottomColor:'rgba(200,200,200,1)', borderBottomWidth: 1}}>
                <Text style={{color: 'gray', fontWeight:'600'}}>الأقل سعرا</Text>
              </TouchableHighlight>
              <TouchableHighlight
                underlayColor={'transparent'}
                onPress={()=>this.onPressSortOption()}
                style={{flex:0.30,justifyContent:'center', alignItems:'center'}}>
                <Text style={{color: 'gray', fontWeight:'600'}}>الأكثر تفضيلا</Text>
              </TouchableHighlight>
            </View>
          </Modal>
      );
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner visible={this.state.loading} />
        {/*{this.renderTabMenu()}*/}
        { this.state.showData &&
            this.renderTabInfo()
        }
        <View style={{height: 60}} />
        {this.renderSortModal()}
        <BottomMenu selectedTab={1} onPressTab={(tab)=>this.onPressTab(tab)}/>
      </View>
    );
  }
}

export default HomeScreen;