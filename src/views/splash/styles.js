import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent:'center'
  },
  imageSplash: {
    position: 'absolute',
    right:0,
    left:0 ,
    bottom: 0,
    top:0,
    width:'100%',
    height:'100%'
  },
  bottomBtns: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    right:0,
    left:0 ,
    bottom: 0,
    height: 100
  },
  btnSignUp: {
    height: 45,
    flex:0.50,
    backgroundColor:'white',
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 8,
    borderColor:'rgba(247,153,84,1)',
    borderWidth:1
  },
  btnLogin: {
    height: 45,
    flex:0.50,
    backgroundColor:'rgba(70,70,70,1)',
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 8
  }
});

export default styles;