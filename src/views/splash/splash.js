import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation';

import styles from './styles';

class SplashScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props){
    super(props);
    AsyncStorage.getItem('userData').then((data)=>{
      if(data){
        let userData = JSON.parse(data);
        if(userData.id){
          this.goToHome();
        }
      }
    })
  }

  goToHome(){
    this.props.navigation.navigate('Home');
  }

  onPressSignUp(){
    this.props.navigation.navigate('Register');
  }

  onPressLogin(){
    this.props.navigation.navigate('Login');
  }

  render() {
    return (
      <View style={styles.container}>
        <Image resizeMode={'contain'} source={require('../../static/images/splash-msg-wo-msg.png')} style={styles.imageSplash} />
        <Text style={{fontWeight:'600', fontSize: 15, marginTop: 180, fontFamily:'system font'}}>هل تودا ادراج برنامج للدليل ؟</Text>
        <View style={styles.bottomBtns}>
          <View style={{flex:0.10}}></View>
          <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressSignUp()} style={styles.btnSignUp}>
            <Text style={{fontWeight:'600', fontSize: 15}}>عضو جديد</Text>
          </TouchableHighlight>
          <View style={{flex:0.05}}></View>
          <TouchableHighlight underlayColor='transparent' onPress={()=> this.onPressLogin()} style={styles.btnLogin}>
            <Text style={{color:'white',fontWeight:'600', fontSize: 15}}>الدخول</Text>
          </TouchableHighlight>
          <View style={{flex:0.10}}></View>
        </View>
      </View>
    );
  }
}

export default SplashScreen;