import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

class BottomMenu extends Component {

    constructor(props){
        super(props);
        const selectedTab = this.props.selectedTab;
        let tabs = {
            tab1: require('../static/images/tab1-a.png'),
            tab2: require('../static/images/tab2-d.png'),
            tab3: require('../static/images/tab3-de.png'),
            tab4: require('../static/images/tab4-d.png')
        }
        switch(selectedTab) {
            case 1 :
                tabs = {
                    tab1: require('../static/images/tab1-a.png'),
                    tab2: require('../static/images/tab2-d.png'),
                    tab3: require('../static/images/tab3-de.png'),
                    tab4: require('../static/images/tab4-d.png')
                }
                break;
            case 2 :
                tabs = {
                    tab1: require('../static/images/tab1-d.png'),
                    tab2: require('../static/images/tab2-a.png'),
                    tab3: require('../static/images/tab3-de.png'),
                    tab4: require('../static/images/tab4-d.png')
                }
                break;
            case 3 :
                tabs = {
                    tab1: require('../static/images/tab1-d.png'),
                    tab2: require('../static/images/tab2-d.png'),
                    tab3: require('../static/images/tab3-ac.png'),
                    tab4: require('../static/images/tab4-d.png')
                }
                break;
            case 4 :
                tabs = {
                    tab1: require('../static/images/tab1-d.png'),
                    tab2: require('../static/images/tab2-d.png'),
                    tab3: require('../static/images/tab3-de.png'),
                    tab4: require('../static/images/tab4-a.png')
                }
                break;
            default:
                tabs = tabs;
                break;
        }
        this.state = { tabs: tabs};
    }

    onPressTab(tab) {
        this.props.onPressTab(tab);
    }

    render() {
        return (
            <View style={styles.viewBottomMenu}>
                <TouchableHighlight
                    underlayColor='transparent'
                    style={styles.btnBottomMenu}
                    onPress={()=> this.onPressTab(4)}
                >
                    <Image resizeMode={'contain'} style={styles.imgBottomMenu} source={this.state.tabs.tab4} />
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor='transparent'
                    style={styles.btnBottomMenu}
                    onPress={()=> this.onPressTab(3)}
                >
                    <Image resizeMode={'contain'} style={styles.imgBottomMenu} source={this.state.tabs.tab3} />
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor='transparent'
                    style={styles.btnBottomMenu}
                    onPress={()=> this.onPressTab(2)}
                >
                    <Image resizeMode={'contain'} style={styles.imgBottomMenu} source={this.state.tabs.tab2} />
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor='transparent'
                    style={styles.btnBottomMenu}
                    onPress={()=> this.onPressTab(1)}
                >
                    <Image resizeMode={'contain'} style={styles.imgBottomMenu} source={this.state.tabs.tab1} />
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewBottomMenu: {
        flex: 1,
        flexDirection: 'row',
        position: 'absolute',
        height: 60,
        bottom: 0,
        right:0,
        left: 0,
        backgroundColor:'white'
    },
    btnBottomMenu: {
        flex:0.25,
        alignItems:'center',
        justifyContent:'center'
    },
    imgBottomMenu: {
        width: 25,
        height: 25
    }
});

const propTypes = {
    selectedTab: PropTypes.number,
    onPressTab: PropTypes.func,
}

BottomMenu.PropTypes = propTypes;

export default BottomMenu;