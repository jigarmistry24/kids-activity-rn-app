import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  ListView,
  StyleSheet,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';

class Activities extends Component {

    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        const activityData = this.props.activityData;
        this.state = {
            listViewKey : Math.random(),
            ds: ds,
            favIds: [],
            fromFav: this.props.fromFav,
            activityData: activityData,
            activityDataSource: ds.cloneWithRows(activityData),
        }
    }

    componentDidMount(){
        AsyncStorage.getItem('favIds').then((data) => {
            if(data){
                let favIds = JSON.parse(data);
                this.setState({favIds: favIds});
            }
        });
    }

    renderFavImage(data){
        let check = this.state.favIds.filter((id)=>{
            return id == data._id;
        });
        if(check.length > 0){
            return (
                <Image resizeMode={'contain'} style={{width: 20, height: 20, marginTop: 10, marginLeft: 5}} source={require('../static/images/active-heart.png')} />
            );
        }else{
            return (
                <Image resizeMode={'contain'} style={{width: 20, height: 20, marginTop: 10, marginLeft: 5}} source={require('../static/images/deactive-heart.png')} />
            );
        }
    }

    onPressFavIcon(data){
        AsyncStorage.getItem('favIds').then((ids) => {
            var favIds = [];
            var activityData = this.state.activityData;
            if(ids){
                favIds = JSON.parse(ids);
                let check = favIds.filter((id)=>{
                    return id != data._id;
                });
                if(check.length != favIds.length){
                    if(this.state.fromFav){
                        let filterActivityData = activityData.filter((adata)=>{
                            return adata._id != data._id;
                        });
                        activityData = filterActivityData;
                    }
                    favIds = check;
                }else{
                    favIds.push(data._id);
                }
            }else{
                favIds = [];
                favIds.push(data._id);
            }
            this.setState({
                favIds: favIds,
                activityData: activityData,
                activityDataSource: this.state.ds.cloneWithRows(activityData)
            });
            AsyncStorage.setItem('favIds', JSON.stringify(favIds));
        });
    }

    renderActivities(data, rowId){
        return (
            <View style={styles.viewActivity}>
                <View style={{flex: 1, flexDirection:'row', margin: 10}}>
                    <TouchableHighlight underlayColor={'transparent'} style={{flex:0.10}} onPress={()=>this.onPressFavIcon(data)}>
                        {this.renderFavImage(data)}
                    </TouchableHighlight>
                    <View style={styles.viewActivityDetail} onPress={()=>this.props.onPressActivity()}>
                        <Text style={{textAlign:'right',fontWeight:'600',  marginRight: 20, marginBottom: 10, fontSize: 13}}>
                            {data.name}
                        </Text>
                        <Text style={{textAlign:'right',fontWeight:'600', marginRight: 20, color:'gray', fontSize: 12}}>
                            {data.academy.name}
                        </Text>
                    </View>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={{flex:0.25, justifyContent: 'center', alignItems:'center'}}
                        onPress={()=>this.props.onPressActivity(data)}>
                        <Image resizeMode={'contain'} style={{width: 60, height: 60}} source={require('../static/images/oragr.png')} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    render() {
        return (
            <ListView
                key={this.state.listViewKey}
                dataSource={this.state.activityDataSource}
                removeClippedSubviews={false}
                enableEmptySections
                renderRow={(data, sectionId, rowId) => this.renderActivities(data, rowId)}
            />
        );
    }
}

const styles = StyleSheet.create({
    viewActivity: {
        height: 85,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom:10,
        backgroundColor: 'white',
        borderRadius: 10
    },
    viewActivityDetail: {
        marginRight: 1,
        alignItems:'flex-end',
        justifyContent:'center',
        flex:0.65,
        borderRightWidth: 1,
        borderRightColor:'rgba(240,240,240,1)'
    }
});

const propTypes = {
    activityData: PropTypes.array,
    onPressActivity: PropTypes.func
}

Activities.PropTypes = propTypes;

export default Activities;