import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import SplashScreen from './views/splash/splash';
import LoginScreen from './views/login/login';
import RegisterScreen from './views/register/register';
import RegisterChildScreen from './views/register/register-child';
import HomeScreen from './views/home/home';
import SearchScreen from './views/home/search';
import FavouriteScreen from './views/favourite/favourite';
import ActivityDetailScreen from './views/activity/activity-detail';
import AcademyScreen from './views/activity/academy';
import BlogScreen from './views/blog/blog';
import BlogDetailScreen from './views/blog/blog-detail';

console.disableYellowBox = true;

export const KidsActivities = StackNavigator({
  Splash: { screen: SplashScreen },
  Login: { screen: LoginScreen },
  Register: { screen: RegisterScreen },
  RegisterChild: { screen: RegisterChildScreen },
  Home: { screen: HomeScreen },
  Search: { screen: SearchScreen },
  ActivityDetail: { screen: ActivityDetailScreen },
  Favourite: { screen: FavouriteScreen },
  Academy: { screen: AcademyScreen },
  Blog: { screen: BlogScreen },
  BlogDetail: { screen: BlogDetailScreen },
});

AppRegistry.registerComponent('KidsActivities', () => KidsActivities);

