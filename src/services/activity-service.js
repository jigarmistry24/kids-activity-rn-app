import React from 'react';
import config from '../helpers/config';

var Activity = module.exports = {
    getActivities: function () {
        let url = config.urls.api_url + config.urls.activity;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'GET',
            headers: headers,
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    },
    getAcademyDetail: function (id) {
        let url = config.urls.api_url + config.urls.academy_detail + "/" + id;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'GET',
            headers: headers,
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    },
    getAcademyActivities: function (id) {
        let url = config.urls.api_url + config.urls.academy_activities + "/" + id;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'GET',
            headers: headers,
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    },
    getBlukActivites: function (data) {
        let url = config.urls.api_url + config.urls.activity_filter;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    }
}

export default Activity;