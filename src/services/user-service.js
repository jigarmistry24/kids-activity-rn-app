import config from '../helpers/config';

var User = module.exports = {
    registerUser: function (data) {
        let url = config.urls.api_url + config.urls.register;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    },
    loginUser: function (data) {
        let url = config.urls.api_url + config.urls.login;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    }
}

export default User;