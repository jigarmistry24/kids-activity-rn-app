import React from 'react';
import config from '../helpers/config';

var Blog = module.exports = {
    getBlogs: function () {
        let url = config.urls.api_url + config.urls.blog;

        var headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("apitoken", config.urls.api_token);

        var request = new Request(url, {
            method: 'GET',
            headers: headers,
        });

        return fetch(request).then(function (res) {
            return res.json();
        }).catch(function (err){
            return err;
        });
    }
}

export default Blog;