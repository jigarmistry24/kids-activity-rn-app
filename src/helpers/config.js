const config = {
    "urls": {
        "api_url": "http://localhost:9000",
        "api_token": 'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9',
        "login": "/api/user/mobile/login",
        "register": "/api/user/mobile",
        "activity": "/api/activity",
        "academy_detail": "/api/academy",
        "academy_activities": "/api/activity/academy",
        "blog": "/api/blog",
        "activity_filter": "/api/activity/filter"
    }
}
export default config;