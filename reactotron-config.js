import {NativeModules} from 'react-native';
import Reactotron from 'reactotron-react-native';

let DEV_MODE = true;

if(DEV_MODE) {

  let scriptHostname;
  
  const scriptURL = NativeModules.SourceCode.scriptURL;
  scriptHostname = scriptURL.split('://')[1].split(':')[0];

  Reactotron
    .configure({host: scriptHostname, name: 'Kids Activity'}) // controls connection & communication settings
    .useReactNative() // add all built-in react native plugins
    .connect() // let's connect!

  console.tron = Reactotron;

  console.show = (name, value) => {
    console.tron.display({
      name,
      value,
      important: true,
    });
  };

}
